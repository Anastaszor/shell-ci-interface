<?php declare(strict_types=1);

/*
 * This file is part of the anastaszor/shell-ci-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Test;

/**
 * Tests the CI class.
 *
 * @author Anastaszor
 */
interface CIInterface
{
	
	/**
	 * Test for the self_accessor phpcs rule. Should keep this interface name
	 * as return value and as return hint for phpdoc.
	 * 
	 * @return CIInterface
	 */
	public function getSelf() : CIInterface;
	
	/**
	 * Tests the CI.
	 */
	public function run() : string;
	
}
