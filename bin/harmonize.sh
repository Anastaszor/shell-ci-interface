#!/bin/bash
set -eu
IFS=$'\n\t'

# {{{ FUNCTIONS

# Writes into the console the date and a message
# @var $1 string the message to write
log() {
	echo -e "[$(date '+%Y-%m-%d %H:%M:%S')] $1"
}

# Writes into the console the given string in WHITE
# @var $1 string the message to write
log_info() {
	log "[  INFO   ] $1"
}

# Writes into the console the given string in RED
# @var $1 string the message to write
log_error() {
	log "[  \033[31mERROR\033[0m  ] $1"
}

# Writes into the console the given string in GREEN
# @var $1 string the message to write
log_success() {
	log "[ \033[32mSUCCESS\033[0m ] $1"
}

# Writes into the console the given string in YELLOW
log_warning() {
	log "[ \033[33mWARNING\033[0m ] $1"
}

# }}} FUNCTIONS


CURRDIR="$(dirname "$0")"
CURRDIR="$(realpath "$CURRDIR/..")"
TODAY=$(date +%Y-%m-%d)
GITMESG="Harmonization $TODAY Updated psalm version constraints"

if [ -n "${1-}" ] # $1 is set and non empty
then
	GITMESG="$1"
fi

bash "$CURRDIR/runtests.sh"

REPODIR="$(realpath "$CURRDIR/..")"

# loop on all repositories that are found next this one
for REPO in $(php "$CURRDIR/bin/order_repositories.php")
do
	# ignore repositories that are not code repositories
	REPONAME=$(basename "$REPO")
	
	if [[ "$REPONAME" == $(basename "$REPODIR") ]]
	then
		continue
	fi
	
	# ignore all directories that do not start with :
	[ "${REPONAME:0:3}" != "php" ] &&
	[ "${REPONAME:0:3}" != "yii" ] &&
	[ "${REPONAME:0:3}" != "mag" ] &&
	[ "${REPONAME:0:3}" != "mtg" ] &&
	[ "${REPONAME:0:4}" != "poly" ] && continue
	
	cd "$REPO"
	log_info "cd $REPO"
	git pull --all
	
	# copy .editorconfig file from this repo to all the repos
	cp "$CURRDIR/.editorconfig" "$REPO/.editorconfig"
	# copy .gitignore file from this repo to all the repos
	cp "$CURRDIR/.gitignore" "$REPO/.gitignore"
	# copy .gitlab-ci.yml from this repo to all the repos
	cp "$CURRDIR/.gitlab-ci.yml" "$REPO/.gitlab-ci.yml"
	# copy .php-cs-fixer.dist.php from this repo to all the repos
	rm -f "$REPO/.php_cs.dist"
	cp "$CURRDIR/.php-cs-fixer.dist.php" "$REPO/.php-cs-fixer.dist.php"
	# copy license file from this repo to all the repos
	cp "$CURRDIR/LICENSE" "$REPO/LICENSE"
	# copy phpmd.xml file from this repo to all the repos
	cp "$CURRDIR/phpmd.xml" "$REPO/phpmd.xml"
	# copy phpstan.neon file from this repo to all the repos
	cp "$CURRDIR/phpstan.neon" "$REPO/phpstan.neon"
	# copy psalm.xml file from this repo to all the repos
	cp "$CURRDIR/psalm.xml" "$REPO/psalm.xml"
	# copy runtests.sh file from this repo to all the repos
	cp "$CURRDIR/runtests.sh" "$REPO/runtests.sh"
	# copy phpunit.xml file from this repo to all the repos
	cp "$CURRDIR/phpunit.xml" "$REPO/phpunit.xml"
	
	git add --all
	set +e
	# https://stackoverflow.com/questions/3882838/whats-an-easy-way-to-detect-modified-files-in-a-git-workspace
	git commit -m "$GITMESG"
	STATUS=$?
	set -e
	log_info "STATUS: $STATUS"
	if [ $STATUS == 0 ]
	then
		
		bash "$REPO/runtests.sh"
		
		git add --all
		set +e
		git commit -m "$GITMESG"
		GITTAG=$(git describe --abbrev=0 --tags)
		HASTAG=$?
		set -e
		if [ $HASTAG == 0 ]
		then
			# Update patch digit
			NEWMAJOR=$(php -r "echo explode('.', '$GITTAG')[0];")
			NEWMINOR=$(php -r "echo explode('.', '$GITTAG')[1];")
            NEWPATCH=$(php -r "echo explode('.', '$GITTAG')[2];")
			if (( NEWMAJOR < 8 )) && (( NEWMAJOR > 1 ))
			then
				NEWTAG="8.0.0"
			#elif (( NEWMAJOR > 8 ))
			#then
			#	NEXTMINOR=$(( NEWMINOR + 1))
			#	NEWTAG="$NEWMAJOR.$NEXTMINOR.0"
			else
				NEXTPATCH=$(( NEWPATCH + 1 ))
				NEWTAG="$NEWMAJOR.$NEWMINOR.$NEXTPATCH"
			fi
            log_info "TAG : $GITTAG => $NEWTAG"
			git tag "$NEWTAG" -m "$GITMESG"
			git push origin
			git push origin "$NEWTAG"
		fi
	fi
	
done
