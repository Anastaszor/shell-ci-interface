#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR/..")

REPODIR=$(realpath "$CURRDIR/..")
TODAY=$(date +%Y-%m-%d)

# loop on all repositories that are found next this one
for REPO in $(find "$REPODIR" -maxdepth 1 -type d | sort -r)
do
	# ignore repositories that are not code repositories
	REPONAME=$(basename "$REPO")
	
	if [[ "$REPONAME" == $(basename "$REPODIR") ]]
	then
		continue
	fi
	
	# ignore all directories that do not start with :
	[ ${REPONAME:0:3} != "php" ] &&
	[ ${REPONAME:0:3} != "yii" ] &&
	[ ${REPONAME:0:3} != "mag" ] &&
	[ ${REPONAME:0:3} != "mtg" ] &&
	[ ${REPONAME:0:4} != "poly" ] && continue
	
	echo "Processing $REPO"
	cd "$REPO"
	echo "cd $REPO"
	
	REMOTE=$(git config --get remote.origin.url)
	# if the remote is https remote, then get the domain from it
	if [[ "$REMOTE" == "https://*" ]]
	then
		DOMAIN=$(echo "$REMOTE" | awk -F/ '{print $3}')
	else
		DOMAIN=$(echo "$REMOTE" | awk -F@ '{print $2}' | awk -F: '{print $1}')
	fi
	REPOPATH=$(cat "$REPO/composer.json" | jq '.name' | sed 's/"//g')
	
	# ignore if repo does not exists
	set +e
	git pull --all
	set -e
	git push --all "git@$DOMAIN:$REPOPATH.git"
	git push --tags "git@$DOMAIN:$REPOPATH.git"
	
done
