#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR="$(dirname "$0")"
CURRDIR="$(realpath "$CURRDIR/..")"
TODAY=$(date +%Y-%m-%d)

REPODIR="$(realpath "$CURRDIR/..")"

# loop on all repositories that are found next this one
for REPO in $(find "$REPODIR" -maxdepth 1 -type d | sort -r)
do
	# ignore repositories that are not code repositories
	REPONAME=$(basename "$REPO")
	
	if [[ "$REPONAME" == $(basename "$REPODIR") ]]
	then
		continue
	fi
	
	# ignore all directories that do not start with :
	[ ${REPONAME:0:3} != "php" ] &&
	[ ${REPONAME:0:3} != "yii" ] &&
	[ ${REPONAME:0:3} != "mag" ] &&
	[ ${REPONAME:0:3} != "mtg" ] &&
	[ ${REPONAME:0:4} != "poly" ] && continue
	
	REPOPATH=$(cat "$REPO/composer.json" | jq '.name' | sed 's/"//g')
	echo "REPOPATH : $REPOPATH"
	
	cd "$REPO"
	git pull --all
	
	# copy .editorconfig file from this repo to all the repos
	cp -v "$CURRDIR/.editorconfig" "$REPO/.editorconfig"
	# copy .gitignore file from this repo to all the repos
	cp -v "$CURRDIR/.gitignore" "$REPO/.gitignore"
	# copy .gitlab-ci.yml from this repo to all the repos
	cp -v "$CURRDIR/.gitlab-ci.yml" "$REPO/.gitlab-ci.yml"
	# copy .php-cs-fixer.dist.php from this repo to all the repos
	rm -fv "$REPO/.php_cs.dist"
	cp -v "$CURRDIR/.php-cs-fixer.dist.php" "$REPO/.php-cs-fixer.dist.php"
	# copy license file from this repo to all the repos
	cp -v "$CURRDIR/LICENSE" "$REPO/LICENSE"
	# copy phpmd.xml file from this repo to all the repos
	cp -v "$CURRDIR/phpmd.xml" "$REPO/phpmd.xml"
	# copy phpstan.neon file from this repo to all the repos
	cp -v "$CURRDIR/phpstan.neon" "$REPO/phpstan.neon"
	# copy psalm.xml file from this repo to all the repos
	cp -v "$CURRDIR/psalm.xml" "$REPO/psalm.xml"
	# copy runtests.sh file from this repo to all the repos
	cp -v "$CURRDIR/runtests.sh" "$REPO/runtests.sh"
	
done
